# Load this software stack into your environment by sourcing this file:
#
#   %>  source deepcam_env.sh
#
# The N10_DEEPCAM environment variable will determine the location of the dependencies
# It should be set to the directory that contains the DeepCAM README.md 

export N10_DEEPCAM=FIXME
if [[ -z "${N10_DEEPCAM}" ]]; then
  echo "The N10_DEEPCAM environment variable has not been set!"
  echo "Edit deepcam_env.sh to define N10_DEEPCAM and try again."
  return 1
fi

prepend_env () {
    # This function is needed since trailing colons
    # on some environment variables can cause major
    # problems...
    local envname=$1
    local envval=$2
    if [ "x${!envname}" = "x" ]; then
        export ${envname}="${envval}"
    else
        export ${envname}="${envval}":${!envname}
    fi
}

load_deepcam () {

    # Location of the software stack
    export PREFIX=${N10_DEEPCAM}/local
    export BUILD=${N10_DEEPCAM}/local/build
    mkdir -p $PREFIX 
    mkdir -p $BUILD 

    # Add software stack to the environment
    prepend_env "PATH" "${PREFIX}/bin"
    prepend_env "CPATH" "${PREFIX}/include"
    prepend_env "LD_LIBRARY_PATH" "${PREFIX}/lib"

    #extract python major.minor version
    pysite=$(python3 --version 2>&1 | sed -e "s#Python \(.*\)\.\(.*\)\..*#\1.\2#")
    prepend_env "PYTHONPATH" "${PREFIX}/lib/python${pysite}/site-packages"
}

load_deepcam
if [[ ! -e ${PREFIX}/pyenv/bin/activate ]]; then
  python3 -m venv --upgrade-deps ${PREFIX}/pyenv
fi
source ${PREFIX}/pyenv/bin/activate


