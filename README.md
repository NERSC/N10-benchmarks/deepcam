This repository describes the DeepCAM AI benchmark
from the [NERSC-10 (N10) Workflow benchmark suite](https://www.nersc.gov/systems/nersc-10/benchmarks). <br>
The [N10 benchmark run rules](https://gitlab.com/NERSC/N10-benchmarks/run-rules-and-ssi/-/blob/main/N10_Benchmark_RunRules.pdf) 
should be reviewed before running this benchmark.<br>
In particular, note:
- The N10 run rules apply to DeepCAM except where explicitly noted within this README.
- The run rules define "baseline", "ported" and "optimized" categories of performance optimization.
   These categories are superceded by the "baseline" and "optimized" definitions in [Section 1](#1-allowed-optimizations) below.
   There is no "ported" category for DeepCAM.
- The projected walltime for the target problem on the target system must not exceed the reference time 
measured by running  the reference problem on NERSC's Perlmutter system.
Concurrency adjustments (i.e. weak- or strong-scaling) may be needed to match the reference time.
**The constraint on the projected walltime does not apply to the baseline category.**

- Responses to the N10 RFP should include performance estimates for the baseline category;
   results for the "optimized" category are optional. 


# 0. DeepCAM Overview

The DeepCAM benchmark trains a deep learning model to identify extreme weather
phenomena (tropical cyclones, atmospheric rivers) in
[CAM5 climate simulation data](http://www.cesm.ucar.edu/models/cesm1.0/cam/docs/description/cam5_desc.pdf).

The N10 DeepCAM benchmark is based on the MLPerf HPC DeepCAM benchmark, derived from the 
2018 ACM Gordon Bell prize-winning work 
[*Exascale Deep Learning for Climate Analytics*](https://arxiv.org/abs/1810.01993). 
It is a semantic segmentation deep learning application
but has particular relevance to HPC because it uses high resolution (768x1152) scientific
images produced from HPC simulations that have more channels (16) than are typically found in
commercial use-cases (3 channels for RGB images).
The MLPerf HPC DeepCAM reference implementation is maintained by [MLCommons](https://mlcommons.org),
and provides, to the extent possible, an architecture-agnostic code base for DeepCAM. 
The N10 DeepCAM benchmark, referred to as the "baseline implementation",
is a fork of the official MLPerf HPC reference implementation
with a few minor modifications.

# 1. Allowed Optimizations

The performance categories ("baseline" and "optimized") for the DeepCAM benchmark depend on the 
[MLPerfHPC training policies](https://github.com/mlcommons/training_policies/blob/master/hpc_training_rules.adoc),
and in turn, the non-HPC
[MLPerf training policies](https://github.com/mlcommons/training_policies/blob/master/training_rules.adoc).
All stipulations of the N10 run rules
that are not specified by the MLPerf policies
continue to apply to DeepCAM.

**Baseline**:
The [baseline implementation of DeepCAM](https://github.com/NERSC/mlperf-hpc/tree/n10-deepcam/deepcam)
must be used as-is, or with minimally-required code updates.
Additionally, the hyperparameter settings (e.g. batch size, learning rate) must match
the configuration in [benchmark/bench_rcp.conf](benchmark/bench_rcp.conf)

**Ported**: There is no ported category for DeepCAM.

**Optimized**: Optimized implementations of DeepCAM must follow the MLPerf HPC closed division rules for allowed code changes and hyperparameter settings.
- The benchmark may be re-implemented in another framework (e.g. TensorFlow)
- The training data may be stored in an alternative format (e.g. conversion from HDF5 to Numpy).
- Reduced precision numerical formats may be used as allowed by the
   [MLPerf training policies](https://github.com/mlcommons/training_policies/blob/master/training_rules.adoc#52-numerical-formats).
- Hyperparameter settings may be modified as described in the
  [MLPerf HPC training policies](https://github.com/mlcommons/training_policies/blob/master/hpc_training_rules.adoc#51-hyperparameters-and-optimizer).
- All other aspects of the optimized implementation (e.g. model architecture, optimizer and dataset sampling)
   must be mathematically equivalent to the baseline implementation.

For both categories, the model must be trained to the target quality metric of IOU > 0.80 as measured on the evaluation dataset.
The convergence criteria may require training beyond 16 epochs,
but benchmark performance will be evaluated using the time to complete 16 training epochs,
regardless of the number of steps needed to converge.

**Optimized_pm example**:
[The LBNL (NERSC) DeepCAM submission for MLPerf HPC v1.0](https://github.com/mlcommons/hpc_results_v1.0/tree/master/LBNL/benchmarks/deepcam/implementations)
is an example of a optimized implementation that has been tuned for Perlmutter.
This implementation is provided to document how performance was obtained
on [NERSC's Perlmutter system](https://www.nersc.gov/systems/perlmutter).
It includes optimizations that are not necessarily portable. 
It is also a vehicle to identify and demonstrate several methods
that may be used to accelerate other optimized implementations, namely: 

<a name="PM_Optimizations"></a>
- NVIDIA Data Loading Library ([DALI](https://developer.nvidia.com/dali)) for accelerating the data pipelines
- Fast data staging from all-flash shared filesystem into on-node DRAM
- PyTorch JIT compilation
- Asynchronous task-graph programming using
   [CUDA graphs](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#cuda-graphs)
- Shifter containers based on [NGC PyTorch](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/pytorch)

# 2. Installing DeepCAM

<a name="N10_DEEPCAM"></a>
Before beginning, it is convenient to store the name of the directory
that contains this `README.md`  file in the `N10_DEEPCAM` variable,
and update the deepcam_env.sh script with this value:
```
export N10_DEEPCAM=$(pwd)
sed -i s%FIXME%$N10_DEEPCAM% deepcam_env.sh
```

The DeepCAM benchmark is implemented in the Pytorch framework and its source code is written in Python. 
Therefore, no compilation is needed for the DeepCAM code,
but Pytorch and other dependent Python packages and libraries must be installed

The basic steps to install DeepCAM are:
1. Obtain the DeepCAM source code
2. Prepare the software environment to perform the build
3. Install the compiled libraries used by DeepCAM
4. Install the Python packages used by DeepCAM

**The `build_scripts` directory contains a few build scripts and Dockerfiles.
They are provided for convenience and are not intended to prescribe how to build DeepCAM. 
They may be modified as needed to build DeepCAM for different architectures or compilers.**

The next few subsections describe a "bare-metal" build of DeepCAM, corresponding to the generic build script 
`build_scripts/quick_install.sh`, or the Perlmutter build script, `build_scripts/install_all_pm.sh`.
Alternatively, DeepCAM can be installed using containers;
to do so, skip to [Section 2.5](#25-build-a-deepcam-container-optional). 

## 2.1 Obtain the DeepCAM source code

The following commands will download the baseline implementation of DeepCAM
from <https://github.com/NERSC/mlperf-hpc.git>
and save it in the `baseline` directory.
The MLperf repository includs a considerable volume of data that is not used by DeepCAM.
The sparse checkout script will retrieve only the DeepCAM source code.
```
cd $N10_DEEPCAM
build_scripts/sparse_checkout.sh baseline
```
The DeepCAM training script for the baseline implementation is
`$N10_DEEPCAM/baseline/src_deepCam/train.py`. 

**Optimized_pm example**: 
The following commands will download the Perlmutter-optimized implementation of DeepCAM
from <https://github.com/mlcommons/hpc_results_v1.0.git> 
and save it in the `optimized_pm` directory.
```
cd $N10_DEEPCAM
build_scripts/sparse_checkout.sh optimized_pm
```
The DeepCAM training script for this implementation is
`optimized_pm/src_deepCam/train.py`.


## 2.2 Prepare the software environment

The DeepCAM environment is most easily set up using Python-3.9 or newer.
If a suitable version of Python is not already present,
then it can be installed using the provided script:
```
cd $N10_DEEPCAM
build_scripts/install_python.sh
```

To prepare the DeepCAM build environment, first verify that `deepcam_env.sh`
has been modified to define `N10_DEEPCAM` variable as described [above](#N10_DEEPCAM).
Then load the DeepCAM environment by running
```
source $N10_DEEPCAM/deepcam_env.sh
```
This script prepares the `local` directory where the required libraries will be installed 
and sets the environment variables needed to build and run the benchmark.
It also creates a Python virtual environment that permits the installation of Python packages 
without affecting system-wide Python settings.
The same command (`deepcam_env.sh`) may also be used when returning to the DeepCAM environment in a new session.
It additionally defines a bash function,  `deactivate`, to exit the Python virtual environment.

## 2.3 Install the compiled libraries used by DeepCAM

DeepCAM depends on the software packages listed in the table below.
These should be installed before the Python package dependencies.
These packages are often pre-installed in system directories;
if not, then it is convenient to install them into the prefix `$N10_DEEPCAM/local`
so that they will be automatically added to the search path set up by `deepcam_env.sh`.
Example scripts for downloading and installing these packages are located in the `$N10_DEEPCAM/build_scripts`.
They have been configured to use the [GNU compilers](https://gcc.gnu.org/)
and should be modified as needed to suit the target environment. 


| Package <br> & Link | Version | Description | Installation <br> Script |
|---                  |---      |---          |---                       |
| [MPI](https://www.mpi-forum.org/docs/)       | MPI-3.0 standard  | Message passing interface <br> Many other implementations can be used  <br> (e.g., [MPICH](https://www.mpich.org), [OpenMPI](https://www.open-mpi.org),etc.) | install_mpich.sh |
| [python](https://www.python.org)      | >= 3.9                   | Python programming language | install_python.sh |
| [hdf5](https://www.hdfgroup.org)      | >= 1.8.4                 | High-performance I/O functions using the HDF5 file format |`install_hdf5.sh`  |
| [glog](https://github.com/google/glog)| >= 0.6.0                 | Application-level logging | install_glog.sh |


**Optimized_pm example**: 
The Perlmutter-optimized implementation uses additional packages to take advantage of NVIDIA GPUs.
These packages are not required by the baseline implementation,
and were provided by the base image of the Perlmutter-optimized container (`nvcr.io/nvidia/pytorch:21.12-py3`).
| Package <br>& Link                       | Tested Version | Description                             |
|---                                       | ---            | ---                                     |
| [CUDA](https://developer.nvidia.com/cuda-toolkit)|    11.5  | Programming interface for NVIDIA GPUs   |
| [NCCL](https://developer.nvidia.com/nccl)        |  2.11.4  | NVIDIA collective communication library |
| [cuDNN](https://developer.nvidia.com/cudnn)      |   8.2.4  | Primitives for deep neural networks     |
| [APEX](https://github.com/NVIDIA/apex)           | 89cc215a | Utilities for distributed training      |

## 2.4 Install the Python packages used by DeepCAM

Most of the Python packages used by DeepCAM are listed in the `build_scrips/requirements.txt` file.
These packages are subject to ongoing development,
and version requirements among them may change.
The mutual compatibility of the package versions listed in    
`requirements.txt` has been verified.
Other versions may be used, but their compatibility cannot be guaranteed.
The following command will install these packages simultaneously:
```
pip install -r $N10_DEEPCAM/build_scripts/requirements.txt
```

Several other Python packages should be compiled from source.
Pip installations of these packages would download generic versions the base libraries,
which could forego architecture specific optimizations.
| Package <br> & Link                              | Tested <br> Version | Description                               |
|---                                                         |---        |---                                        |
| [mpi4py](https://www.github.com/mpi4py/mpi4py)             | 3.0.3     | Python interface to the MPI  library      |
| [h5py](https://www.h5py.org)                               | 3.2.1     | Python interface to the HDF5 library      |
| [PyTorch](https://pytorch.org)                             | 1.11.0    | Deep learning framework                   |


The mpi4py and h5py packages should be configured
to use the MPI and HDF5 libraries provided by the system
(or installed during the previous step).
Refer to the instructions on the package websites
([mpi4py](https://mpi4py.readthedocs.io/en/stable/install.html#using-distutils),
 [h5py](https://docs.h5py.org/en/stable/build.html#custom-install) ).

PyTorch must be installed from the source because
the generic pip installation does not provide the MPI support required by DeepCAM.
See `build_scripts/install_pytorch.sh` for an example
of building  a **minimal** Pytorch installation with MPI support,
but **without architecture-specific libraries**.
For an example of building PyTorch for Perlmutter's GPUs, see `build_scripts/install_pytorch_pm.sh`.
Either is likely to require modification in order to support the target architecture.
Detailed instructions for other architectures are available on the
[PyTorch github site](https://github.com/pytorch/pytorch#from-source).

Installation of additional architecture-specific libraries
for the ML operators underlying PyTorch is optional,
and permitted for both baseline and optimized categories.
For example, the Perlmutter baseline uses
the [APEX](https://github.com/NVIDIA/apex) package.
See the `build_scripts/install_all_pm.sh` script to install it. 

## 2.5 Build a DeepCAM container (optional)

The DeepCAM  benchmark may optionally be run in a container.
This may be useful for encapsulating the DeepCAM dependencies
or improving job launch time.
This section may be skipped if you choose not to run DeepCAM in a container.

Any container system (e.g. Kubernetes, Virtual Box, etc.) may be used.
An example using the [Shifter/Docker](https://docs.nersc.gov/development/shifter/) 
system is provided,
but this is not intended to convey a requirement or preference.
A few `Dockerfiles` are provided in the `build_scripts` directory.
To build the container from the Dockerfile,
run the following commands (after obtaining the DeepCAM source code as described in 
[Section 2.1](#21-obtain-the-deepcam-source-code)):
```
cd $N10_DEEPCAM/mlhperf_hpc/deepcam
docker build --tag deepcam:latest -f $N10_DEEPCAM/build_scripts/Dockerfile.Reference_APEX  .
```

# 3. Running DeepCAM

## 3.1 Obtain input data

Training data for the DeepCAM benchmark comes from 
[CAM5](https://www.cesm.ucar.edu/models/cam) simulations. 
The samples are stored in HDF5 files with input images of shape (768, 1152, 16) 
and pixel-level labels of shape (768, 1152).
The labels have three target classes (background, atmospheric river, tropical cyclone)
and were produced with [TECA](https://github.com/LBL-EESA/TECA).


Two sets of training data are provided:
a "mini" dataset to aid preliminary testing and profiling, and
the full dataset required to run the benchmark.
The download and storage requirements are listed in the following table.
| Data-set  | Download Size | Storage|
|---        |---            |---     |
| Per-image |           N/A |  61 MB |
| Mini      |         24 GB |  75 GB |
| Full      |        8.8 TB | 8.8 TB |


The data are hosted at NERSC,
and the recommended way to obtain it is to use 
[Globus](https://docs.nersc.gov/services/globus).
The data is hosted at NERSC and is accessible from this Globus 
[endpoint](https://app.globus.org/file-manager?origin_id=0b226e2c-4de0-11ea-971a-021304b0cca7).
Detailed instructions are in the file [`data/globus.md`](data/globus.md).

No further steps are needed to prepare the full dataset.
The mini dataset requires the following step
to copy images into the directory structure required by DeepCAM:

```
cd $N10_DEEPCAM/data
tar -xzf deepcam-data-mini/deepcam-data-n512.tgz
./install_data.sh deepcam-data-n512 mini 1
```

## 3.2 Reformat input data (optional)

The Perlmutter-optimized implementation requires that the dataset be reformatted into Numpy files.
The conversion from HDF5 to Numpy can be done with the script `data/convert_hdf52npy.py`.
The script is MPI-parallel for faster conversion.
This conversion is not necessary for the baseline implementation.
```
cd $N10_DEEPCAM/data
./convert_hdf52npy.py --input_directory All-Hist/train    --output_directory All-Hist/numpy/train
./convert_hdf52npy.py --input_directory All-Hist/validate --output_directory All-Hist/numpy/validate
```

## 3.3 Benchmark execution

### 3.3.1 "Bare-metal" execution 

The `${N10_DEEPCAM}/benchmarks` directory includes two examples of
batch scripts for running DeepCAM on Perlmutter.
The `run_training_mini.slurm` script 
should be used only for debugging and profiling DeepCAM.
The `run_training_nranks1024.slurm` script demonstrates the full benchmark.
These scripts should be modified for
the target architecture, environment (i.e. batch scheduler) and concurrency.

Within these scripts,
several command-line arguments are passed to DeepCAM (`train.py`).
For **baseline** runs,
the hyperparameters specified in
`benchmark/bench_rcp.conf`](benchmark/bench_rcp.conf) should not be modified.
Within the slurm script, the local batch size (`--local_batch_size`) may be adjusted
under the constraint that the global batch size is fixed to 2048,
i.e. `processes * local_batch_size = 2048`.
Other options specified in the slurm script may be adjusted freely, including:

| Option | Description | Comment |
|---     |---          |---      |
| `--data_dir_prefix` | Path to the input dataset | $N10_DEEPCAM/data |
| `--output_dir`      | Path to output files      | |
| `--max_inter_threads` | Number of concurrent I/O threads | May be tuned for the target architecture |
| `--wireup_method`     | Communication interface |  Valid settings are ["dummy", "nccl-openmpi", <br> "nccl-slurm", "nccl-slurm-pmi", "mpi"].|
| `--optimizer`| Optimization algorithm | AdamW or LAMB |
| `--max_epochs` | Maximum number of epochs | Any integer sufficient for convergence. |


For **optimized** runs,
some of the hyperparameters in `bench_rcp.conf` may be adjusted as follows.
Arguments that influence the optimizer's convergence rate may be modified
(e.g.  `--start_lr 0.0055`, `--lr_schedule`, `--weight_decay` ).
Arguments that control the frequency or locality of operations
( `--save_frequency`,
  `--gradient_accumulation_frequency`,
  `--logging_frequency` and
  `--batchnorm_group_size`)
should not be modified.

### 3.3.2 Container execution (optional)

To start the container and run the DeepCAM benchmark,
see [`benchmarks/run_training_nranks1024_shifter.slurm`](benchmarks/run_training_nranks1024_shifter.slurm)
and [`benchmarks/run_shifter.sh`](benchmarks/run_shifter.sh)
for an examples of how to incorporate these commands into a batch script 
on Perlmutter, which uses Slurm and Shifter. 
The following two lines accomplish this:

```
#SBATCH --image=docker:deepcam:latest
srun <srun options> shifter <shifter/docker options> bash ./run_shifter.sh 
```
`SBATCH --image` is a custom option added by NERSC's shifter plugin, 
which pulls the specified docker image.
The srun command launches the container.
The run_shifter.sh script runs within the container invokes the DeepCAM executable (train.py). 


If your system supports Docker, you can run DeepCAM as:
```
mpirun <mpirun options> docker run deepcam:latest bash ./run_shifter.sh 
```

**Container Example**  
The Perlmutter-optimized DeepCAM was run under a container to obtain the MLPerf HPC v1.0 results.
Use the following commands to run the optimized implementation
under a container on 512 nodes of Perlmutter:
```
cd $N10_DEEPCAM/optimized_pm/LBNL/benchmarks/deepcam/implementations/deepcam-pytorch
source configs/config_pm_512x4x1.sh
sbatch -N 512 -t 00:30:00 run_pm.sub
```

# 4. Results

## 4.1 Correctness and timing

Correct results must converge to a target quality requirement of `IOU >= 0.80`
computed on the evaluation dataset in a manner consistent with the DeepCAM baseline implementation.
The quality metric must also be reported via the standard MLPerf logging
in a manner consistent with baseline.
This corresponds to the MLPerf HPC definition of correctness with a slightly relaxed IOU requirement (`IOU >=0.80` vs `0.82`).
Convergence may require more than the 16 epochs of training for the timing measurement described below.
Jobs will run for at least 16 epochs, then continue until `--max_epochs` or convergence is reached, whichever comes first.

The `Benchmark Time` measurement is evaluated using the time to train the model for 16 epochs.
We follow the MLPerf HPC rules for timing with the exception that the timer stops at the end of the 16th epoch, rather than at convergence.
The runtime is computed from timestamps in the log messages,
starting at the `run_start` message
and ending at the `epoch_stop` message that corresponds to the 16th epoch.
In accordance with the MLPerf HPC rules, the dataset must start in a parallel shared filesystem;
any data staging to node-local storage must be included in the measured time.


The `parse_result.sh` script should be used to extract
the validation test and `Benchmark Time` from the job's log file.
For example:
```
> ./parse_result.sh result_1.txt
| Validation: PASSED
| Benchmark Time: 99.245 seconds 
```

## 4.2 Performance on Perlmutter

The table below lists runtimes measured on NERSC's Perlmutter GPU system.
Perlmutter's  GPU nodes have one AMD Milan CPU and four NVIDIA 40GB A100 GPUs.
Each job used four MPI tasks per node, each with one GPU and 16 CPU cores.

As in previous sections, results are shown for both
a baseline implementation
(with the APEX library of ML primitive operators)
and an example Perlmutter-optimized implementation.
The `Benchmark Time` corresponds to the time  to complete 16 training epochs.

| Implementation | Nodes | Benchmark <br> Time (sec) |
|---             |---    |---    |
| Baseline       |   256 | 240.0 |
| Baseline       |   512 | 204.0 |
| Optimized      |   512 |  99.2* |

The reference time that determines a minimum performance requirement
for the target system is indicated with an asterisk ('*') in the table.  
Note that the reference time is obtained from the Perlmutter-optimized implementation.

<!--**will be**  indicated by an asterisk ('*').
**The reference time will be determined before the final release of the benchmark.**-->


## 4.3 Reporting

Benchmark results should include projections of the `Benchmark Time`
for the full problem size.
The hardware configuration (i.e. the number of elements from each pool of computational resources)
needed to achieve the estimated timings must also be provided.
For example, if the target system includes more than one type of compute node,
then report the number and type of nodes used.
If the target system enables disaggregation/composability,
a finer-grained resource list is needed,
as described in the Workflow-SSI document.

For the electronic submission,
include the MLPerf logs along with the implementation source code and instructions to
install the software environment.


