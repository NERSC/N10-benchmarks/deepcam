#!/bin/bash
#install pytorch from source to enable MPI

set -e

if [[ -z "${N10_DEEPCAM}" ]]; then
  echo "The N10_DEEPCAM environment variable should be set before running  install_pytorch_pm.sh"
  exit 1
fi

source ${N10_DEEPCAM}/deepcam_env.sh
#deepcam_env.sh defines two envs: PREFIX and BUILD

cd $BUILD
git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
#git checkout v2.0.0  #compilation error with torch/csrc/profiler/kineto_client_interface.cpp
git checkout b32afbb  #commit date Mar 23, 2023  


glog_prefix=$N10_DEEPCAM/local
export USE_ROCM=0
export USE_CUDNN=1
export USE_MKLDNN=0
export USE_FBGEMM=0
export USE_DISTRIBUTED=1
export USE_MPI=1
export USE_OPENMP=1
export CC=cc
export CXX=CC
export CXXFLAGS=$CFLAGS
export LDFLAGS="-Wl,-zmuldefs -Wl,-rpath=$N10_DEEPCAM/local/lib -Wl,-rpath=$N10_DEEPCAM/local/lib64"
export LD_LIBRARY_PATH=$glog_prefix/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$glog_prefix/lib64:$LIBRARY_PATH
export CMAKE_PREFIX_PATH=${MPICH_DIR}:$CMAKE_PREFIX_PATH
export CMAKE_PREFIX_PATH=/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/math_libs/11.7:$CMAKE_PREFIX_PATH
#export USE_SYSTEM_NCCL=0
export NCCL_ROOT=${CRAY_CUDATOOLKIT_DIR}/nccl
export CMAKE_BUILD_PARALLEL_LEVEL=16

python3 setup.py install


