#!/bin/bash
#This script builds DeepCAM on Perlmutter using gcc/11.2.0.
set -e

export N10_DEEPCAM=$SCRATCH/n10/deepcam

#use python/3.9-anaconda-2021.11
module load python  

source deepcam_env.sh

#use BUILD and PREFIX defined in deepcam_env.sh
#PREFIX=$N10_DEEPCAM/local
#BUILD=$N10_DEEPCAM/local/build
#BUILD=/tmp/$USR

#use cray-mpich/8.1.24 

#use cray-hdf5/1.12.2.3 module
module load cray-hdf5


#install glog 0.6.0 using cmake 3.20.4 from /usr/bin
cd $BUILD   
wget https://github.com/google/glog/archive/refs/tags/v0.6.0.tar.gz
tar xvzf v0.6.0.tar.gz
cd glog-0.6.0
cmake -S . -B build -G "Unix Makefiles"
cmake --build build -j16
cmake --install build --prefix $PREFIX


#install Python dependencies
pip install -r $N10_DEEPCAM/build_scripts/requirements.txt


#build mpi4py 3.1.4 from soure to use Cray MPICH
cd $BUILD
wget https://github.com/mpi4py/mpi4py/archive/refs/tags/3.1.4.tar.gz
tar -xzf 3.1.4.tar.gz
cd mpi4py-3.1.4
export CC=cc
export MPICC=cc
python3 ./setup.py build
python3 ./setup.py install
  

#install pytorch from source to enable MPI
cd $BUILD
git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
#git checkout v2.0.0  #compilation error with torch/csrc/profiler/kineto_client_interface.cpp
git checkout b32afbb #commit date Mar 23, 2023  

#use CUDA_HOME=/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/cuda/11.7 (module load cudatoolkit/11.7)
#use NCCL_ROOT=/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/comm_libs/11.7/nccl
#alternatively, use NCCL from /global/common/software/nersc/pm-2022q4/sw/nccl-2.15.5-ofi-r4 (module load nccl/2.15.5-ofi)

glog_prefix=$N10_DEEPCAM/local
export USE_ROCM=0
export USE_CUDNN=1
export USE_MKLDNN=0
export USE_FBGEMM=0
export USE_DISTRIBUTED=1
export USE_MPI=1
export USE_OPENMP=1
export CXX=CC
export CC=cc
export CXXFLAGS=$CFLAGS
export LDFLAGS="-Wl,-zmuldefs -Wl,-rpath=$N10_DEEPCAM/local/lib -Wl,-rpath=$N10_DEEPCAM/local/lib64"
export LD_LIBRARY_PATH=$glog_prefix/lib64:$LD_LIBRARY_PATH
export LIBRARY_PATH=$glog_prefix/lib64:$LIBRARY_PATH
export CMAKE_PREFIX_PATH=${MPICH_DIR}:$CMAKE_PREFIX_PATH
export CMAKE_PREFIX_PATH=/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/math_libs/11.7:$CMAKE_PREFIX_PATH
#export USE_SYSTEM_NCCL=0
export NCCL_ROOT=/opt/nvidia/hpc_sdk/Linux_x86_64/22.5/comm_libs/11.7/nccl
export CMAKE_BUILD_PARALLEL_LEVEL=16

python3 setup.py install


#instal apex from source
cd $BUILD
git clone https://github.com/NVIDIA/apex
cd apex  
#git checkout 22.03  #runtime error: NVIDIA APEX not found
git checkout 89cc215a #commit 89cc215a Mar 30 22:29:18 2023
pip install -r requirements.txt
pip install -v --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./


#cleanup
#rm -fr $BUILD
