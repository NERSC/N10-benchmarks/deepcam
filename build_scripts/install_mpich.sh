#!/bin/bash
#set -x
set -e

if [[ -z "${N10_DEEPCAM}" ]]; then
  echo "The N10_DEEPCAM environment variable should be set before running  install_mpich.sh"
  exit 1 
fi
source ${N10_DEEPCAM}/deepcam_env.sh
#deepcam_env.sh defines two envs: PREFIX and BUILD

cd ${BUILD}
mpich_version=4.0.2
mpich_prefix=mpich-$mpich_version


echo "Fetching MPICH..."
wget https://www.mpich.org/static/downloads/$mpich_version/$mpich_prefix.tar.gz

echo "Building MPICH..."
tar xvzf $mpich_prefix.tar.gz
cd $mpich_prefix
FFLAGS="-fallow-argument-mismatch" FCFLAGS="-fallow-argument-mismatch -O3 -fPIC -pthread" ./configure --prefix=${PREFIX}
make -j 16
make install
