The baseline Dockerfile that uses the APEX library
( [`Dockerfile.baseline_APEX`](def)) was obtained from
[here](https://github.com/NERSC/mlperf-hpc/blob/main/deepcam/docker/Dockerfile).

[`Dockerfile.baseline_noAPEX`](def) is equivalent to `Dockerfile.baseline_APEX`
but does not use APEX.

The Perlmutter-optimized Dockerfile
([`Dockerfile.optimized_pm`](def)) was obtained from
[here](https://github.com/mlcommons/hpc_results_v1.0/blob/master/NVIDIA/benchmarks/deepcam/implementations/pytorch/Dockerfile).
