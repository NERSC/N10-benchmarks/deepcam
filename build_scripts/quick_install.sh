#!/bin/bash
#
# This script installs DeepCAM and its dependencies
# by performing the steps in README.md .
# The script should be called from the N10_DEEPCAM directory (see README.md).
#
# It assumes the following software is already present
# MPI,  cmake >= 3.22.0, python >= 3.9
# On Perlmutter, these are provided by:
module load cmake/3.22.0
module load python/3.9-anaconda-2021.11
#module load pytorch/1.9.0
#
# also assumes that the N10_DEEPCAM environment variable has been set
# N10_DEEPCAM will be tested (or set by) deepcam_env.sh

#set environment variables to describe deepcam install dirs
#and create the python virtual environment
source build_scripts/deepcam_env.sh
cd ${N10_DEEPCAM}

#install non-python dependencies - uncomment as needed
#perlmutter provides mpi and hdf5 modules
#build_scripts/install_hdf5.sh
#build_scripts/install_mpich.sh
build_scripts/install_glog.sh

#to get MPI support, pytorch must be installed from source
#perlmutter  provides a pytorch module
#build_scripts/install_pytorch.sh

#other python packages
#pip install -r requirements.txt
#pip install -r optional_nvidia.txt


#mlperf_git=https://github.com/NERSC/mlperf-hpc.git
#git clone --single-branch -b n10-deepcam $mlperf_git
#git checkout 957c107

