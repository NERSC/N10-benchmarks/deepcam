#!/bin/bash
cd /benchmark

#load hyperparameters
#BENCH_RCP is defined by bench_rcp.conf
#this reference convergence point (RCP) should not be modified
source /benchmark/bench_rcp.conf

python3 /opt/deepCam/train.py \
     ${BENCH_RCP_BASELINE} \
    --wireup_method "nccl-slurm" \
    --run_tag ${run_tag} \
    --data_dir_prefix $DATADIR \
    --output_dir ${output_dir} \
    --model_prefix "segmentation" \
    --optimizer "LAMB" \
    --max_epochs $MAX_EPOCHS \
    --max_inter_threads $MAX_INTER_THREADS \
    --local_batch_size ${local_batch_size}

