# Download input data using Globus

The recommended way to get the input data is to use 
[Globus](https://www.globus.org/). 
You must be able to log in to the Globus web interface using one of the supported credentials listed on the [Globus login page](https://bit.ly/3m5QhfI). In addition, you must have access to a destination endpoint to download the data. If you are new to Globus, you can get started with an online tutorial. In addition, You may find the [NERSC Globus document](https://docs.nersc.gov/services/globus/) useful.

You can download the DeepCAM input data by following the instructions below:

* Use the following link to open the Globus web interface and log in.<br>
https://app.globus.org/file-manager?origin_id=0b226e2c-4de0-11ea-971a-021304b0cca7&origin_path=%2F.

* The link will take you to the File Manager page,
and the "Climate segmentation benchmark data" collection will be visible on the left-hand side of the screen. If you land on a single-pane view upon login, you can click the two-pane `Panels` option on the upper right corner to switch the view.  

* Use the upper right text box to search for and select your destination Globus endpoint. Use the second text box in the upper right to select the destination directory for the transfer.
The run scripts within the `benchmark` directory are set-up to find the `$N10_DEEPCAM/data` directory,
but alternate locations may be more suitable for capacity or performance reasons.

* From the table on the left, select "All-Hist" (for the full 8.8 TB data set)  or "deepcam-data-mini" (for the mini 24 GB data set).
Click the "Start (>)" button in the upper left to initiate the transfer.
A pop-up will appear to confirm that the transfer task was accepted
The transfer progress can be monitored using the "Activity Monitor" interface
(found on the left edge of the browser window).
You will receive an email when the transfer is complete.




